const express = require('express');
const router = express.Router();
const redis = require("redis");
const crypto = require('crypto');
const Sidekiq = require('sidekiq');

var redis_cache_pool = [
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL),
  redis.createClient(process.env.REDIS_CACHE_URL)
];
var redis_cache_pool_index = 0;

//var redis_cache = redis.createClient(process.env.REDIS_CACHE_URL);
var redis_jobs = redis.createClient(process.env.REDIS_JOBS_URL);
var sidekiq = new Sidekiq(redis_jobs, process.env.NODE_ENV);

console.log("inicio do controller")

/* GET users listing. */
router.get('/:sale_system_name/:api_key/product/:remote_code', function(req, res, next) {
  redis_cache = redis_cache_pool[redis_cache_pool_index];
  redis_cache_pool_index = redis_cache_pool_index + 1;
  if(redis_cache_pool_index >= redis_cache_pool.length) { redis_cache_pool_index = 0 }


  var build_product_key = function(sale_system_id, remote_code){
    return "cache:shippingproduct:"+sale_system_id+":"+remote_code;
  };

  var build_sale_system_key = function(sale_system_id, api_key){
    return "translator:sale_system_id:"+sale_system_id+":"+api_key;
  };

  var build_lead_time_key = function(sale_system_id){
    return "translator:"+sale_system_id+":use_lead_time";
  };

  var create_job = function(api_key, sale_system_name, remote_code){
    sidekiq.enqueue("TranslateProductWorker", [api_key, sale_system_name, remote_code], {
      retry: true,
      queue: "translator"
    });
  };

  var renderResponse = function(hash){
    res.status(200);
    res.json(hash);
  };

  var createJob = function(){
    create_job(req.params.api_key, req.params.sale_system_name, req.params.remote_code);
    res.status(404);
    res.json({error: 'Não foi possível encontrar remote_code: ' + req.params.remote_code});
  };

  var getProduct = function(sale_system_id, use_lead_time){
    redis_cache.get( build_product_key(sale_system_id, req.params.remote_code), function(err, product){
      if(!product) return createJob();

      hash = JSON.parse(product)
      hash['lead_time'] = use_lead_time ? hash['lead_time'] : 0

      renderResponse(hash)
    });
  };

  var getLeadTime = function(sale_system_id){
    if(!sale_system_id) return createJob();

    redis_cache.get( build_lead_time_key(sale_system_id), function(err, lead_time){
      if(lead_time === undefined || lead_time === null ) return createJob();
      getProduct(sale_system_id, lead_time === 1)
    });
  };
  
  redis_cache.get( build_sale_system_key(req.params.sale_system_name, req.params.api_key), function(err, sale_system_id){
    getLeadTime(sale_system_id);
  });

});

module.exports = router;
